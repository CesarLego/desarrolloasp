﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Clientes.Models
{
    public class Customers
    {
        [Key]
        public int Cid { get; set; }

        [Required(ErrorMessage = "Ingresa el Nombre del cliente")]
        [Display(Name = "Nombre del cliente")]
        public string Cname { get; set; }

        [Required(ErrorMessage = "Ingresa el email del cliente")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Ingresa un email válido")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Edad del cliente")]
        [Display(Name = "Edad")]
        [Range(20, 50)]
        public int Age { get; set; }

        [Required(ErrorMessage = "Ingresa el monto de apertura")]
        [Display(Name = "Monto de apertura")]
        public int Saldo{ get; set; }
    }
}
