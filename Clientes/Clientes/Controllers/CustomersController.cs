﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clientes.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Clientes.Controllers
{
    public class CustomersController : Controller
    {
        private readonly ClientesContext _context;

        public CustomersController(ClientesContext context)
        {
            _context = context;


        }

        [HttpGet]
        public async Task<IActionResult> Index(string empsearch)
        {
            ViewData["GetEmployeeDetails"] = empsearch;
            var empquery = from x in _context.Customers select x;

            if (!String.IsNullOrEmpty(empsearch))
            {
                empquery = empquery.Where(x => x.Cname.Contains(empsearch) ||
                x.Email.Contains(empsearch));
            }

            return View(await empquery.AsNoTracking().ToListAsync());
        }

        public IActionResult Index()
        {
            var displaydata = _context.Customers.ToList();
            return View(displaydata);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Customers nCliente)
        {
            if (ModelState.IsValid)
            {
                _context.Add(nCliente);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nCliente);
        }

        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var getDetail = await _context.Customers.FindAsync(id);

            return View(getDetail);
        }

        public async Task<IActionResult> Actualizar(int? id)
        {
            if (id==null)
            {
                return RedirectToAction("Index");
            }

            var getEmpDetail = await _context.Customers.FindAsync(id);

            return View(getEmpDetail);

        }

        [HttpPost]
        public async Task<IActionResult> Actualizar(Customers actualCustomer)
        {
            if(ModelState.IsValid)
            {
                _context.Update(actualCustomer);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(actualCustomer);

        }

        public async Task<IActionResult> Eliminar(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var getEmpDetail = await _context.Customers.FindAsync(id);

            return View(getEmpDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Eliminar(int id)
        {
            var getEmpdetail = await _context.Customers.FindAsync(id);
            _context.Customers.Remove(getEmpdetail);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }
    }
}
