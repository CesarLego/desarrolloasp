﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ejemplo2.Models;

namespace Ejemplo2.Controllers
{
    public class EmpleadosController : Controller
    {
        private readonly Ejemplo2Context _context;

        public EmpleadosController(Ejemplo2Context context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            var displaydata = _context.Employee.ToList();
            return View(displaydata);
            
        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Employee nEmp)
        {
            if (ModelState.IsValid)
            {
                _context.Add(nEmp);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nEmp);
        }



    }
}
