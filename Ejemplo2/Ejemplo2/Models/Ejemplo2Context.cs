﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Ejemplo2.Models
{
    public class Ejemplo2Context : DbContext
    {
        public Ejemplo2Context(DbContextOptions<Ejemplo2Context>options): base(options)
        {
        }

        public DbSet<Employee> Employee { get; set; }
    }
}
